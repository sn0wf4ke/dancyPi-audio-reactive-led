import os
broker_url = 'redis://redis:6379/0' if 'CI' in os.environ else 'redis://127.0.0.1:6379/0'
result_backend = broker_url.strip('/0')
