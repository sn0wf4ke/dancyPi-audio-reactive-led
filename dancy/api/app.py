import json

import dancy.api
from dancy.api import factory
from dancy.lights import visualization
from dancy.lights.states import States
from dancy.lights.visualization import handle
from dancy.lights.visualizations import Visualizations
from flask import Response
from flask.globals import request

app = factory.create_app(celery=dancy.api.celery)

@app.route('/api/v1/visualization', methods=['GET'])
def get_status():
    data = visualization.handle({'status': None})
    clear_data = to_clear_data(data)
    json_data = json.dumps(clear_data)
    response = Response(json_data, status=200, mimetype="application/json")
    return response

@app.route('/api/v1/visualization', methods=['POST'])
def set_status():
    response = Response(status=400, mimetype="application/json")
    json_data = request.json
    if json_data is not None:
        data = json_data
        if "visualization" in data:
            if "state" in data:
                if data['visualization'].upper() in Visualizations.__members__:
                    if data['state'].upper() in States.__members__:
                        enum_data = to_enum_data(data)
                        return_value = visualization.handle(enum_data)
                        clear_data = to_clear_data(return_value)
                        json_data = json.dumps(clear_data)
                        response.status_code = 200
                        response.data = json_data
    return response

def to_clear_data(data):
    normalized_data = {key: data[key] for key in ('visualization', 'state')}
    return_data = { key: Visualizations(value).name 
    if key == 'visualization' else States(value).name 
    for (key,value) in normalized_data.items() }
    return {**data, **return_data}

def to_enum_data(data):
    return { key: Visualizations[name].value 
    if key == 'visualization' else States[name].value 
    for (key,name) in data.items() }

if __name__ == '__main__': # pragma: no cover
    app.run(debug=True)
