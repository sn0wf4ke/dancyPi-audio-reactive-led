from celery import Celery
from dancy.api import celery_config

def celery_app(app_name=__name__):
    return Celery(app_name, config_source=celery_config)

celery = celery_app()
