import os
from unittest import TestCase
from unittest.mock import patch

import pytest
import redis
from dancy.api.celery_config import broker_url
from dancy.lights import microphone, visualization
from dancy.lights.states import States
from dancy.lights.visualization import (celery, handle, start,
                                        visualize_energy, visualize_scroll,
                                        visualize_spectrum)
from dancy.lights.visualizations import Visualizations


@pytest.mark.usefixtures('celery_app')
@pytest.mark.usefixtures('celery_worker')
class VisualizationTestCase(TestCase):
  state_matrix = list(map(lambda x: (x, [item for item in States.__members__]), Visualizations.__members__))
  def setUp(self,):
    self.redis = redis.from_url(broker_url, encoding="utf-8", decode_responses=True)
    self.redis.delete('visualization.pid')

  def tearDown(self):
    self.redis.delete('visualization.pid')

  def test_handle(self):
    data = {}
    result = handle(data)
    self.assertIsNotNone(result)

  def test_nothing_changed(self):
    data = {
      'state': States.STOPPED.value,
      'visualization': Visualizations.SCROLL.value
    }
    response = handle(data)
    self.assertDictEqual(data, response)

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.lights.led.update')
  def test_handle_running(self, mock_update, mock_start):
    data = {
      'state': States.RUNNING.value
    }
    result = handle(data)
    self.assertEqual(States.RUNNING.value, result['state'])
    redis_data = self.redis.hgetall('visualization.pid')
    self.assertEqual(result['pid'], redis_data['pid'])


  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.lights.led.update')
  def test_handle_running_when_running(self, mock_update, mock_start):
    data = {
      'state': States.RUNNING.value
    }
    result = handle(data)
    self.assertEqual(States.RUNNING.value, result['state'])
    self.assertTrue(self.redis.exists('visualization.pid'))
    redis_pid = self.redis.hget('visualization.pid', 'pid')
    self.assertEqual(result['pid'], redis_pid)
    result = handle(data)
    self.assertEqual(States.RUNNING.value, result['state'])
    self.assertEqual(result['pid'], redis_pid)


  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.api.celery.control.revoke')
  @patch('dancy.lights.led.update')
  def test_handle_running_when_running_with_different_visualization(
    self, mock_update, mock_revoke, mock_start):
    data = {
      'state': States.RUNNING.value,
      'visualization': Visualizations.ENERGY.value
    }
    result = handle(data)
    self.assertTrue(self.redis.exists('visualization.pid'))
    pid = result['pid']
    redis_pid = self.redis.hget('visualization.pid', 'pid')
    self.assertEqual(pid, redis_pid)
    data['visualization'] = Visualizations.SCROLL.value
    result = handle(data)
    mock_revoke.assert_called_with(pid, terminate=True, signal='SIGKILL')
    redis_pid = self.redis.hget('visualization.pid', 'pid')
    self.assertNotEqual(pid, redis_pid)
    self.assertEqual(result['pid'], redis_pid)

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.api.celery.control.revoke')
  @patch('dancy.lights.led.update')
  def test_handle_running_when_running_with_same_visualization(
    self, mock_update, mock_revoke, mock_start):
    data = {
      'state': States.RUNNING.value,
      'visualization': Visualizations.ENERGY.value
    }
    result = handle(data)
    self.assertEqual(data['visualization'], result['visualization'])
    self.assertEqual(data['state'], result['state'])
    self.assertTrue(self.redis.exists('visualization.pid'))
    pid = result['pid']
    redis_pid = self.redis.hget('visualization.pid', 'pid')
    self.assertEqual(pid, redis_pid)
    result = handle(data)
    self.assertEqual(data['visualization'], result['visualization'])
    self.assertEqual(data['state'], result['state'])
    mock_revoke.assert_not_called()
    redis_pid = self.redis.hget('visualization.pid', 'pid')
    self.assertEqual(pid, redis_pid)
    self.assertEqual(result['pid'], redis_pid)

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.api.celery.control.revoke')
  @patch('dancy.lights.led.update')
  def test_handle_stopped_when_running(
    self, mock_update, mock_revoke, mock_start):
    data = {
      'state': States.RUNNING.value
    }
    result = handle(data)
    self.assertEqual(States.RUNNING.value, result['state'])
    self.assertTrue(self.redis.exists('visualization.pid'))
    data['state'] = States.STOPPED.value
    result = handle(data)
    mock_revoke.assert_called()
    self.assertEqual(result['state'], States.STOPPED.value)
    self.assertFalse(os.path.exists('visualization.pid'))

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.api.celery.control.revoke')
  @patch('dancy.lights.led.update')
  def test_handle_stopped_when_stopped(
    self, mock_update, mock_revoke, mock_start):
    data = {
      'state': States.STOPPED.value
    }
    result = handle(data)
    mock_revoke.assert_not_called()
    self.assertEqual(States.STOPPED.value, result['state'])
    self.assertFalse(self.redis.exists('visualization.pid'))

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.api.celery.control.revoke')
  @patch('dancy.lights.led.update')
  def test_handle_stopped_when_stopped_with_different_visualization(
    self, mock_update, mock_revoke, mock_start):
    data = {
      'state': States.STOPPED.value,
      'visualization': Visualizations.ENERGY.value
    }
    result = handle(data)
    mock_revoke.assert_not_called()
    self.assertEqual(States.STOPPED.value, result['state'])
    self.assertFalse(self.redis.exists('visualization.pid'))

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.lights.led.update')
  def test_handle_visualization(self, mock_update, mock_start):
    for visualization, _ in self.state_matrix:
      with self.subTest(visualization):
        data = {
          'visualization': Visualizations[visualization].value,
          'state': States.RUNNING.value
        }
        result = handle(data)
        self.assertEqual(result['visualization'], Visualizations[visualization].value)


  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.lights.led.update')
  def test_start(self, mock_update, mock_start_stream):
    for name in Visualizations.__members__:
      with self.subTest(name):
        visualization_value = Visualizations[name].value
        start(visualization_value)
        function_name = f'visualize_{name.lower()}'
        function_item = globals()[function_name]
        self.assertEqual(visualization.visualization_effect, function_item)
        mock_update.assert_called()
        mock_start_stream.assert_called()

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.lights.led.update')
  def test_led_update_is_called_on_stop(self, mock_update, mock_start_stream):
    data = {
      'state': States.RUNNING.value
    }
    result = handle(data)
    self.assertEqual(result['state'], States.RUNNING.value)
    data['state'] = States.STOPPED.value
    result = handle(data)
    self.assertEqual(mock_update.call_count, 2)
    mock_start_stream.assert_called_once()
    self.assertEqual(result['state'], States.STOPPED.value)

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.lights.led.update')
  def test_led_update_is_not_called_on_stop_when_stopped(
    self, mock_update, mock_start_stream):
    data = {
      'state': States.STOPPED.value
    }
    result = handle(data)
    self.assertEqual(result['state'], States.STOPPED.value)
    mock_update.assert_not_called()
    mock_start_stream.assert_not_called()
