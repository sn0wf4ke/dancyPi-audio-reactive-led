from unittest import TestCase
from unittest.mock import patch

import pytest
import redis
from dancy.api.app import app
from dancy.api.celery_config import broker_url
from dancy.lights.states import States
from dancy.lights.visualizations import Visualizations


@pytest.mark.usefixtures('celery_app')
@pytest.mark.usefixtures('celery_worker')
class APITestCase(TestCase):
  state_matrix = list(map(lambda x: (x, [item for item in States.__members__]), Visualizations.__members__))

  def setUp(self):
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = False
    self.app = app.test_client()
    self.redis = redis.from_url(broker_url, encoding="utf-8", decode_responses=True)
    self.redis.delete('visualization.pid')

  def tearDown(self):
    self.redis.delete('visualization.pid')

  def test_get_status_when_stopped(self):
    response = self.app.get('/api/v1/visualization', follow_redirects=True)
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.content_type, 'application/json')
    data = response.get_json()
    self.assertIn('state', data)
    self.assertIn('visualization', data)

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.lights.led.update')
  def test_api_handle_get_integration(self, mock_update, mock_start):
    for visualization, states in self.state_matrix:
      for state in states:
        with self.subTest((visualization, state)):
          data={
            "visualization": visualization,
            "state": state
          }
          response = self.app.post(
            '/api/v1/visualization', json=data, follow_redirects=True)
          self.assertEqual(response.status_code, 200)
          response = self.app.get(
            '/api/v1/visualization', follow_redirects=True)
          response_data = response.get_json()
          self.assertEqual(data['state'], response_data['state'])
          if data['state'] == States.RUNNING.name:
            self.assertIn('pid', response_data.keys())
            self.assertEqual(
              data['visualization'], response_data['visualization'])


  def test_set_empty_status(self):
    response = self.app.post('/api/v1/visualization', follow_redirects=True)
    self.assertEqual(response.status_code, 400)
    self.assertEqual(response.content_type, 'application/json')

  def test_set_status(self):
    for visualization, states in self.state_matrix:
      for state in states:
        with self.subTest((visualization, state)):
          with patch(
            'dancy.lights.visualization.handle',
            return_value={
              'visualization': Visualizations[visualization].value,
              'state': States[state].value,
              'pid': 999
              }) as handle:
            data={
              "visualization": visualization,
              "state": state
            }
            response = self.app.post(
              '/api/v1/visualization', json=data, follow_redirects=True)
            self.assertEqual(response.status_code, 200)
            handle.assert_called_with({
              'visualization': Visualizations[visualization].value,
              'state': States[state].value,
              })

  @patch('dancy.lights.microphone.start_stream')
  @patch('dancy.lights.led.update')
  def test_api_handle_set_integration(self, mock_update, mock_start):
    for visualization, states in self.state_matrix:
      for state in states:
        for start_visualization, start_states in self.state_matrix:
          for start_state in start_states:
            with self.subTest(
              (start_visualization, start_state, visualization, state)):
              start_data={
                "visualization": start_visualization,
                "state": start_state
              }
              response = self.app.post(
                '/api/v1/visualization', json=start_data, follow_redirects=True)
              self.assertEqual(response.status_code, 200)
              response_data = response.get_json()
              self.assertEqual(
                start_data['visualization'], response_data['visualization'])
              self.assertEqual(start_data['state'], response_data['state'])
              data={
                "visualization": visualization,
                "state": state
              }
              response = self.app.post(
                '/api/v1/visualization', json=data, follow_redirects=True)
              self.assertEqual(response.status_code, 200)
              response_data = response.get_json()
              self.assertEqual(
                data['visualization'], response_data['visualization'])
              self.assertEqual(data['state'], response_data['state'])
