import pytest
import redis
from dancy.api.celery_config import broker_url, result_backend

@pytest.fixture(scope='session')
def celery_config():
    return {
        'broker_url': broker_url,
        'result_backend': result_backend
    }
