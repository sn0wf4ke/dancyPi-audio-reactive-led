from enum import Enum

class Visualizations(Enum):
    SCROLL = 1
    ENERGY = 2
    SPECTRUM = 3
