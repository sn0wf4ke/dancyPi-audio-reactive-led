from enum import Enum

class States(Enum):
  RUNNING = 1
  STOPPED = 0
